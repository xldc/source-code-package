/*
 *
 *      Copyright (c) 2018-2025, tuanzi All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the tuanzi developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: tuanzi (1766285184@qq.com)
 *
 */

package com.tuanzi.service;

import com.tuanzi.entity.User;
import java.util.List;

/**
 * 描述:
 *
 * @author 团子
 * @date 2021/6/27 10:50 上午
 */
public interface UserService {

  List<User> getUserList();

  void save(User user);

  User findUserById(Long id);

  void edit(User user);

  void delete(Long id);
}
