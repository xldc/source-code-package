package com.tuanzi.test.controller;


import com.tuanzi.test.entity.User;
import com.tuanzi.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 团子
 * @since 2019-05-17
 */
@RestController
@RequestMapping("/test")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/list")
    public Object list(){
        return userService.list();
    }

    @GetMapping("/delete")
    public boolean delete(Integer id){
        return userService.removeById(id);
    }

    @GetMapping("/byid")
    public Object byid(Integer id){
        return userService.getById(id);
    }

    @PostMapping("/update")
    public boolean update(@RequestBody User user){
        return userService.updateById(user);
    }

    @PostMapping("/add")
    public boolean add(@RequestBody User user){
        return userService.save(user);
    }

}
