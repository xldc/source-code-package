package com.tuanzi.test.mapper;

import com.tuanzi.test.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 团子
 * @since 2019-05-17
 */
public interface UserMapper extends BaseMapper<User> {

}
