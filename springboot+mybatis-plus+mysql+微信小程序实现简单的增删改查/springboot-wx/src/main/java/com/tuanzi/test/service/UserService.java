package com.tuanzi.test.service;

import com.tuanzi.test.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 团子
 * @since 2019-05-17
 */
public interface UserService extends IService<User> {

}
