package com.tuanzi.test.service.impl;

import com.tuanzi.test.entity.User;
import com.tuanzi.test.mapper.UserMapper;
import com.tuanzi.test.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 团子
 * @since 2019-05-17
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
