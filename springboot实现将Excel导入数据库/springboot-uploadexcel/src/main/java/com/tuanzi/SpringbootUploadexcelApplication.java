package com.tuanzi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUploadexcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUploadexcelApplication.class, args);
    }

}
