package com.tuanzi.service;


import com.tuanzi.beans.HttpResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface InformationService {

   public HttpResponseEntity getExcelInfo(String fileName, MultipartFile file)throws Exception;
}
