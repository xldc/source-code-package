package com.tuanzi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPushApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootPushApplication.class, args);
    }

}
