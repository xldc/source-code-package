package com.tuanzi.controller;

import com.alibaba.fastjson.JSONObject;
import com.tuanzi.utils.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.tuanzi.utils.RedisUtils.getAccessToken;

/**
 * Created by IntelliJ IDEA.
 *
 * @author 团子
 * description:
 * path: springboot-push-com.tuanzi.controller-PushController
 * date: 2019/7/4 16:55
 */
@RestController
public class PushController {

    private final static Logger logger = LoggerFactory.getLogger(PushController.class);

    /**
     * 功能描述:
     * 微信小程序推送模板消息
     * @param openId 小程序openId
     * @param formId 小程序formId
     * @return
     */
    @GetMapping("/push")
    private boolean push(String openId, String formId){

        logger.info("===========》 pushNoticeUtil方法开始执行");


        JSONObject jsonObject = getAccessToken();

        if (jsonObject.get("access_token") == null || jsonObject.get("access_token").toString() == "") {
            logger.error("================》获取access_token失败");
            return false;
        }
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("touser", openId);
        // template_id 模板Id  微信公众平台添加模板时生成的ID
        jsonObject1.put("template_id", "模板Id");
        jsonObject1.put("form_id", formId);
        JSONObject jsonObject3 = new JSONObject();
        JSONObject jsonObject4 = new JSONObject();
        jsonObject4.put("value","江小白");
        jsonObject3.put("keyword1",jsonObject4);

        jsonObject4 = new JSONObject();
        jsonObject4.put("value","我有一瓶酒，有话对你说");
        jsonObject3.put("keyword2",jsonObject4);

        jsonObject4 = new JSONObject();
        jsonObject4.put("value","请您尽快付款");
        jsonObject3.put("keyword3",jsonObject4);

        jsonObject4 = new JSONObject();
        jsonObject4.put("value","好运来酒店");
        jsonObject3.put("keyword4",jsonObject4);


        jsonObject1.put("data",jsonObject3);

        boolean pushResult = RedisUtils.setPush(jsonObject1.toString(), jsonObject.get("access_token").toString());
        logger.info("=========》 pushNoticeUtil方法结束：推送结果" + pushResult);
        return pushResult;
    }
}
