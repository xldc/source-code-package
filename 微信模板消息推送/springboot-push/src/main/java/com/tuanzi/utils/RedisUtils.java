package com.tuanzi.utils;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by IntelliJ IDEA.
 *
 * @author 团子
 * description:
 * path: springboot-push-com.tuanzi.utils-RedisUtils
 * date: 2019/7/4 16:57
 */

public class RedisUtils {
    private final static Logger logger = LoggerFactory.getLogger(RedisUtils.class);

    /**
     * 获取access_token
     */
    private final static String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&";
    /**
     *推送url
     **/
    private final static String PUSH_URL = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=";

    private final static String APP_ID = "AppID(小程序ID)";

    private final static String SECRET = "AppSecret(小程序密钥)";

    /**
     * 获取access_token
     * @author 团子
     * @return
     */
    public static JSONObject getAccessToken() {
        String url = ACCESS_TOKEN_URL + "appid=" + APP_ID + "&secret=" + SECRET;
        PrintWriter out = null;
        BufferedReader in = null;
        String line;
        StringBuffer sb = new StringBuffer();
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();

            // 设置通用的请求属性 设置请求格式
            //设置返回类型
            conn.setRequestProperty("contentType", "text/plain");
            //设置请求类型
            conn.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            //设置超时时间
            conn.setConnectTimeout(1000);
            conn.setReadTimeout(1000);
            conn.setDoOutput(true);
            conn.connect();
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应    设置接收格式
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF-8"));
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            // 将获得的String对象转为JSON格式
            JSONObject jsonObject = JSONObject.parseObject(sb.toString());
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }


    /**
     * 推送工具类
     * @author 团子
     * @param params 推送消息内容
     * @param accessToken
     * @return boolean
     */
    public static boolean setPush(String params, String accessToken) {
        boolean flag = false;
        String url = PUSH_URL + accessToken;
        OutputStream outputStream = null;
        InputStreamReader inputStreamReader = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        HttpsURLConnection connection = null;
        try {
            // 创建URL对象
            URL realUrl = new URL(url);
            // 打开连接 获取连接对象
            connection = (HttpsURLConnection) realUrl.openConnection();
            // 设置请求编码
            connection.addRequestProperty("encoding", "UTF-8");
            // 设置允许输入
            connection.setDoInput(true);
            // 设置允许输出
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            // 当outputStr不为null时向输出流写数据
            if (null != params) {
                outputStream = connection.getOutputStream();
                // 注意编码格式
                outputStream.write(params.getBytes("UTF-8"));
                outputStream.close();
            }
            // 从输入流读取返回内容
            inputStream = connection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            bufferedReader = new BufferedReader(inputStreamReader);
            String str = null;
            StringBuffer buffer = new StringBuffer();
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            JSONObject jsonObject = JSONObject.parseObject(buffer.toString());
            int errorCode = jsonObject.getInteger("errcode");
            String errorMessage = jsonObject.getString("errmsg");
            if (errorCode == 0) {
                flag = true;
            } else {
                logger.info("模板消息发送失败:" + errorCode + "," + errorMessage);
                flag = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 依次关闭打开的输入流
            try {
                connection.disconnect();
                bufferedReader.close();
                inputStreamReader.close();
                inputStream.close();
                // 依次关闭打开的输出流
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

}
