package com.tuanzi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSignOssApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSignOssApplication.class, args);
    }

}
