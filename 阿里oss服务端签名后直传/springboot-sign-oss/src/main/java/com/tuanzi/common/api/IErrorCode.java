package com.tuanzi.common.api;

/**
 * 封装API的错误码
 * @auther 团子
 * @date 2019-07-31 11:31
 */

public interface IErrorCode {
    long getCode();

    String getMessage();
}
