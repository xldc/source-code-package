package com.tuanzi.service;

import com.tuanzi.dto.OssPolicyResult;

/**
 * oss上传管理Service
 * @auther 团子
 * @date 2019-07-31 11:31
 */

public interface OssService {
    /**
     * oss上传策略生成
     */
    OssPolicyResult policy();
}
