package com.tuanzi.common.api;

import lombok.Data;

/**
 * 用于前后端交互的返回值
 * @auther 团子
 * @date 2019-07-31 11:31
 */

@Data
public class FileUploadResult {
    // 文件唯一标识
    private String uid;
    // 文件名
    private String name;
    // 状态有：uploading done error removed
    private String status;
    // 服务端响应内容，如：'{"status": "success"}'
    private String response;
}
