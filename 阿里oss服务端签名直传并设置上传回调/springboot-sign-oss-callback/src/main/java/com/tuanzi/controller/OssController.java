package com.tuanzi.controller;

import com.tuanzi.common.api.CommonResult;
import com.tuanzi.dto.OssCallbackResult;
import com.tuanzi.dto.OssPolicyResult;
import com.tuanzi.service.impl.OssServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Oss相关操作接口
 * @auther 团子
 * @date 2019-07-31 11:31
 */

@Controller
@Api(tags = "OssController", description = "Oss管理")
@RequestMapping("/aliyun/oss")
public class OssController {

    public static final Logger logger = LoggerFactory.getLogger(OssController.class);

    @Autowired
    private OssServiceImpl ossService;

    @ApiOperation(value = "oss上传签名生成")
    @RequestMapping(value = "/policy", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult< OssPolicyResult > policy() {
        logger.info("policy --------------> start");
        OssPolicyResult result = ossService.policy();
        logger.info("policy --------------> end");
        return CommonResult.success(result);
    }

    @ApiOperation(value = "oss上传成功回调")
    @RequestMapping(value = "/callback", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<OssCallbackResult> callback(HttpServletRequest request) {
        logger.info("callback --------------> start");
        OssCallbackResult ossCallbackResult = ossService.callback(request);
        logger.info("callback --------------> end");
        return CommonResult.success(ossCallbackResult);
    }
}
